// Dependencies.
const io = require('socket.io-client');

import { EventEmitter } from 'events';

// Define API base.
export const DEFAULT_STREAMING_ENDPOINT = `https://stream.api.bitkick.io`;

export interface StreamingClientOptions
{
	jwt?: string,
	endpoint?: string,
}

// Define required options.
const requiredOptions: string[] =
[
	"jwt"
];

// Validate the specified options.
const validateClientOptions = (options: StreamingClientOptions) =>
{
	// Loop through required options.
	requiredOptions.forEach((requiredOption: string) =>
	{
		// Was the required option specified?
		if (!options.hasOwnProperty(requiredOption))
		{
			throw new Error(`${requiredOption} is required`);
		}
	});

	// Set defaults.
	options.endpoint = options.endpoint || DEFAULT_STREAMING_ENDPOINT;
}

// Client for interacting with the streaming API.
export class StreamingClient extends EventEmitter
{
	// Store options.
	protected options: StreamingClientOptions;

	// Store socket.
	protected socket: any;

	// Instantiate and connect.
	constructor(options: StreamingClientOptions)
	{
		// Call parent constructor.
		super();

		// Validate options.
		validateClientOptions(options);

		// Store options.
		this.options = options;

		// Connect.
		this.connect(options.endpoint);
	}

	// Connect to the streaming API.
	protected connect(endpoint: string|undefined): void
	{
		// Validate endpoint.
		if (!endpoint)
			throw `Invalid endpoint`;

		// Connect to streaming API.
		this.socket = io(endpoint, { reconnection: true, transports: ['websocket'] });

		// Listen for socket events.
		this.socket.on('connect', this.onConnect.bind(this));
		this.socket.on('disconnect', this.onDisconnect.bind(this));
		this.socket.on('channelMessage', this.onChannelMessage.bind(this));
		this.socket.on('tick', this.onTick.bind(this));
	}

	// Handle connection.
	protected onConnect(): void
	{
		// Emit.
		this.emit('connect');
	}

	// Handle disconnection.
	protected onDisconnect(reason: any): void
	{
		// Emit.
		this.emit('disconnect', reason);
	}

	// Handle channel messages.
	protected onChannelMessage(message: any): void
	{
		// Emit.
		this.emit('channelMessage', message);
	}

	// Handle tick messages.
	protected onTick(tick: string): void
	{
		// Emit.
		this.emit('tick', tick);
	}

	// Authenticate with the streaming server.
	public async authenticate(jwt?: string): Promise<any>
	{
		return new Promise<any>((resolve, reject) =>
		{
			this.socket.emit('authenticate', {jwt: jwt || this.options.jwt}, (err: any, response: any) =>
			{
				if (err)
				{
					reject(`Authentication error: ${err}`);
				}
				else
				{
					resolve(`Authentication successful: ${response}`);
				}
			});
		});
	}

	// Tell the server that we want to leave all channels.
	public async leaveAllChannels(): Promise<any>
	{
		return new Promise((resolve, reject) =>
		{
			this.socket.emit('leaveAllChannels', (err: any) =>
			{
				if (err)
				{
					reject(`An error occurred while leaving all channels: ${err}`);
				}
				else
				{
					resolve();
				}
			});
		});
	}

	// Tell the server that we want to leave the specified channels.
	public async leaveChannels(channelNames: string[]): Promise<any>
	{
		return new Promise((resolve, reject) =>
		{
			this.socket.emit('leaveChannels', channelNames, (err: any) =>
			{
				if (err)
				{
					reject(`An error occurred while leaving channels: ${err}`);
				}
				else
				{
					resolve();
				}
			});
		});
	}

	// Tell the server that we want to join some channels.
	public async joinChannels(channelNames: string[], returnCacheForMeasurementChannels: boolean = true): Promise<any>
	{
		return new Promise((resolve, reject) =>
		{
			this.socket.emit('joinChannels', channelNames, returnCacheForMeasurementChannels, (err: any) =>
			{
				if (err)
				{
					reject(`An error occurred while joining channels: ${err}`);
				}
				else
				{
					resolve();
				}
			});
		});
	}
}
