const io = require('socket.io-client');
//const url = 'https://staging.stream.api.openmesh.io';
const url = 'http://localhost:3000';
const socket = io(url, { reconnection: false, transports: ['websocket'] });

//const apiKey = 'mock-api-key';
//const apiSecret = 'mock-api-secret';
const apiKey = 'a9ad8126-a557-4df7-9459-8a83a10bfdbd';
const apiSecret = 'fb981a9768e0c86ab70f61d37810ef8447897c11f43996880280f605c4869529fc148970d9d058a09acea60621cdf11a775f6639625e1d1245fc80e33271954e';

// Log startup.
console.info(`Connecting to server: ${url}`);

// Values callback.
const onValues = (err, response) =>
{
	// Handle error.
	if (err)
	{
		// Log it.
		console.warn(`An error occurred while sending data point values:`, err);
		
		// Throw.
		throw `An error occurred while sending data point values: ${err}`;
	}

	// Log it.
	console.info(`Received onValues:`, response);
};

// Auth callback.
const onAuth = (err, response) =>
{
	// Handle error.
	if (err)
	{
		// Log it.
		console.warn(`Authentication error:`, err);

		// Log it.
		throw `Authentication error: ${err}`;
	}

	// Log it.
	console.info(`Authentication successful`);

	// Log it.
	console.info(`Sending values`);

	// Send data point values.
	socket.emit('values', [{DataPointID: '28c135f8-4702-4574-9737-fa13e79c2859', T: '2018-01-02T03:04:05Z', V: 1.2345}], onValues);

	socket.emit('subscribeToDataPointValues', ['data-point-id-1'], onSubscribe);
};

const onSubscribe = (err, response) =>
{
	// Handle error.
	if (err)
	{
		// Log it.
		console.warn(`Subscribe error:`, err);

		// Log it.
		throw `Subscribe error: ${err}`;
	}

	// Log it.
	console.info(`Subscribe successful:`, response);
};

const onUnsubscribe = (err, response) =>
{
	// Handle error.
	if (err)
	{
		// Log it.
		console.warn(`Unsubscribe error:`, err);

		// Log it.
		throw `Unsubscribe error: ${err}`;
	}

	// Log it.
	console.info(`Unsubscribe successful:`, response);
};

// Handle connection.
socket.on('connect', () =>
{
	// Log it.
	console.info(`Connected`);

	// Log it.
	console.info(`Authenticating with API key: ${apiKey} and secret: ${apiSecret}`);

	// Send credentials.
	socket.emit('auth', {apiKey: apiKey, apiSecret: apiSecret}, onAuth);
});

// Handle disconnection.
socket.on('disconnect', (reason) =>
{
	console.info(`Disconnected:`, reason);
});
