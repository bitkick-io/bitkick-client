// Dependencies.
import { expect } from 'chai';
import 'mocha';
const sinon = require('sinon');

import { StreamingClient } from '../lib';

describe('Client', () =>
{
	// Define useful params.
	let jwt: string;

	beforeEach(() =>
	{
		// Reset variables.
	});

	afterEach(() =>
	{
	});

	describe('constructor', () =>
	{
		it('should require a JWT', () =>
		{
			expect(() => {new StreamingClient({}) }).to.throw('jwt is required');
		});
	});
});