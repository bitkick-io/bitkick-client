# bitkick-client

Client library for sending authenticated requests to the streaming API.



## Install

```bash
npm i --save bitkick-client
```



## Build

```bash
npm i
npm run build
```


## Test

```bash
npm test
```
